import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Modal,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';

// React Native Paper Component
import {TextInput} from 'react-native-paper';

// Custome Phone Input by Country Code
import PhoneInput from 'react-native-phone-input';

// Typography
import {material, systemWeights, materialColors} from 'react-native-typography';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalVisibleRegister: false,
      name: '',
    };
  }
  componentDidMount = () => {
    SplashScreen.hide();
    this.checkLogin();
  };

  onChangeText = (value, name) => {
    this.setState({
      [name]: value,
    });
  };

  goToHome = () => {
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: 'JualanAnda',
            params: {
              title: 'Jualan Anda',
              pop: 1,
            },
          },
        ],
      }),
    );
  };

  checkLogin = async () => {
    let value = await AsyncStorage.getItem('data');
    if (value != null) {
      console.log(value);
      this.goToHome();
    }
    console.log(value);
  };

  handleRegular = async () => {
    await AsyncStorage.setItem('regular', 'true');
    this.setState({modalVisible: false});
    this.props.navigation.navigate('Login');
  };

  render() {
    const {modalVisible, modalVisibleRegister, name} = this.state;
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.backgroundImageStyle}
          source={require('../../assets/images/background_image_login.jpg')}>
          <Image
            resizeMode="contain"
            source={require('../../assets/images/logo_horizontal_beri.png')}
          />
          <Image
            style={{marginTop: 20}}
            resizeMode="contain"
            source={require('../../assets/images/onboarding_images.png')}
          />
          <Text
            style={[
              material.headlineWhite,
              systemWeights.semibold,
              {marginTop: 20},
            ]}>
            #MemberiItuAsik
          </Text>
          {/* ------ [START BUTTON CHOICE] ------ */}
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.setState({modalVisible: true});
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={['#FFFFFF', '#FFFFFF', '#FFFFFF']}
              style={styles.button}>
              <Text style={[styles.buttonText, styles.buttonTextColor]}>
                Login
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.setState({modalVisibleRegister: true});
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={['#FFFFFF00', '#FFFFFF00', '#FFFFFF00']}
              style={[styles.button, styles.buttonWithBorderColor]}>
              <Text style={[styles.buttonText, styles.buttonTextColorWhite]}>
                Register
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          {/* ------ [START BUTTON CHOICE] ------ */}
          {/* ------ [] ------ */}
          {/* ------ [START MODAL LOGIN] ------ */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      modalVisible: false,
                    });
                  }}>
                  <Image
                    style={{
                      borderColor: '#16212D',
                      borderWidth: 2,
                      borderRadius: 10,
                      marginBottom: 20,
                    }}
                    source={require('../../assets/images/close.png')}
                  />
                </TouchableOpacity>
                <Text style={[styles.modalText, material.title]}>
                  Welcome Back!
                </Text>
                <Text
                  style={[
                    styles.modalText,
                    material.subheading,
                    {
                      color: materialColors.blackTertiary,
                    },
                  ]}>
                  Enter your mobile number to login.
                </Text>

                <PhoneInput
                  initialCountry="id"
                  autoFormat={true}
                  style={{
                    marginBottom: 20,
                    paddingBottom: 10,
                    borderColor: '#F70161',
                    borderBottomWidth: 1,
                  }}
                  ref={ref => {
                    this.phone = ref;
                  }}
                />

                <TouchableOpacity
                  style={{
                    ...styles.openButton,
                    backgroundColor: '#F70161',
                  }}
                  // onPress={() => this.handleRegular()}
                  onPress={() => alert('under construction')}>
                  <Text style={[styles.textStyle]}>Login</Text>
                </TouchableOpacity>

                <Text
                  style={[
                    material.caption,
                    {textAlign: 'center', marginBottom: 20},
                  ]}>
                  OR
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginBottom: 20,
                  }}>
                  <TouchableOpacity
                    onPress={() => alert('Coming Soon Feature')}>
                    <Image
                      style={{width: 30, height: 30}}
                      source={require('../../assets/images/Google.png')}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => alert('Coming Soon Feature')}>
                    <Image
                      style={{width: 30, height: 30}}
                      source={require('../../assets/images/Facebook_02.png')}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          {/* ------ [END MODAL LOGIN] ------ */}
          {/* ------ [] ------ */}
          {/* ------ [START MODAL REGISTER] ------ */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisibleRegister}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      modalVisibleRegister: false,
                    });
                  }}>
                  <Image
                    style={{
                      borderColor: '#16212D',
                      borderWidth: 2,
                      borderRadius: 10,
                      marginBottom: 20,
                    }}
                    source={require('../../assets/images/close.png')}
                  />
                </TouchableOpacity>
                <Text style={[styles.modalText, material.title]}>
                  Get Started!
                </Text>
                <Text
                  style={[
                    styles.modalText,
                    material.subheading,
                    {
                      color: materialColors.blackTertiary,
                    },
                  ]}>
                  Let's create your account.
                </Text>

                <View style={{flex: 1}}>
                  <TextInput
                    label="Name"
                    value={name}
                    theme={{
                      colors: {
                        primary: '#F70161',
                        underlineColor: 'transparent',
                      },
                    }}
                    onChangeText={text => this.onChangeText(text, 'name')}
                    mode="outlined"
                    style={{
                      marginBottom: 40,
                    }}
                  />

                  <PhoneInput
                    initialCountry="id"
                    autoFormat={true}
                    style={{
                      marginBottom: 20,
                      paddingBottom: 10,
                      borderColor: '#F70161',
                      borderBottomWidth: 1,
                    }}
                    ref={ref => {
                      this.phone = ref;
                    }}
                  />

                  <TouchableOpacity
                    style={{
                      ...styles.openButton,
                      backgroundColor: '#F70161',
                    }}
                    // onPress={() => this.props.navigation.navigate('Login')}
                    onPress={() => alert('under construction')}>
                    <Text style={[styles.textStyle]}>Create Account</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          {/* ------ [END MODAL REGISTER] ------ */}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginBottom: 20,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 20,
    textAlign: 'left',
  },
  backgroundImageStyle: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'column',
  },
  button: {
    borderRadius: 20,
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  buttonWithBorderColor: {
    borderWidth: 2,
    borderColor: 'white',
  },
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
  buttonTextColor: {
    color: '#F70161',
  },
  buttonTextColorWhite: {
    color: '#FFFFFF',
  },
});
